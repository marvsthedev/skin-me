# syntax=docker/dockerfile:1
FROM nikolaik/python-nodejs:python3.10-nodejs16-bullseye as base

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PYTHONFAULTHANDLER=1

RUN pip install pipenv
RUN apt-get update && apt-get install -y --no-install-recommends gcc
RUN corepack enable 
RUN yarn set version stable

WORKDIR /app
COPY . .

RUN pipenv install --system --dev

RUN yarn install

