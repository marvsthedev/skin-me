import React from 'react';
import {
  Routes,
  Route
} from "react-router-dom";

import Layout from "./compontents/Layout";
import LandingPage from "./pages/LandingPage";
import PurchasePage from "./pages/PurchasePage";
import ConfirmationPage from "./pages/ConfirmationPage";
import BillingDetails from "./compontents/BillingDetails";
import ShippingDetails from "./compontents/ShippingDetails";

export default function App() {
  return (
    <Routes>
        <Route path="/" element={<Layout />}>
            <Route index element={<LandingPage />} />
            <Route path="purchase" element={<PurchasePage />}>
                <Route path="shipping-details" element={<ShippingDetails />} />
                <Route path="billing-details" element={<BillingDetails />} />
                <Route path="confirmation" element={<ConfirmationPage />} />
            </Route>

            {/* 
                Using path="*"" means "match anything", so this route
                acts like a catch-all for URLs that we don't have explicit
                routes for. 
            */}
            <Route path="*" element={<LandingPage />} />
        </Route>
    </Routes>
  );
}

