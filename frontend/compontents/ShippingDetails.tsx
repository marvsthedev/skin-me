import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function ShippingDetails() {
    const navigate = useNavigate();

    useEffect(() => {
        document.body.style.backgroundColor = "white";
    }, [])

    return (
        <div>
            <h1>SHIPPING DETAILS</h1>
            <hr />
            <div></div>
            <form action="">
                <div className="field">
                    <label className="label">YOUR EMAIL</label>
                    <div className="control">
                        <input className="input" type="email" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">GIVEN NAME</label>
                    <div className="control">
                        <input className="input" type="text" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">FAMILY NAME</label>
                    <div className="control">
                        <input className="input" type="text" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">FIRST LINE OF ADDRESS</label>
                    <div className="control">
                        <input className="input" type="text" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">SECOND LINE OF ADDRESS</label>
                    <div className="control">
                        <input className="input" type="text" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">TOWN</label>
                    <div className="control">
                        <input className="input" type="text" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field is-grouped">
                    <div className="field mr-5">
                        <label className="label">COUNTRY</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Type Here" />
                        </div>
                    </div>

                    <div className="field ml-5">
                        <label className="label">POST CODE</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Type Here" />
                        </div>
                    </div>
                </div>

                <div className="field is-grouped">
                    <div className="field mr-5">
                        <label className="label">MOBILE</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="+44" disabled />
                        </div>
                    </div>

                    <div className="field ml-5">
                        <label className="label"></label>
                        <div className="control" style={{'top': '25.5%'}}>
                            <input className="input" type="text" placeholder="Type Here" />
                        </div>
                    </div>
                </div>
    
                <div className="field">
                    <label className="checkbox">
                        <input type="checkbox" />
                        Please do not email me exciting Skin + Me special offers.
                    </label>
                </div>

                <div className="field">
                    <label className="checkbox">
                        <input type="checkbox" />
                        Yes, I agree to the Skin + Me <a href="#">Terms of Purchase</a>
                    </label>
                </div>
            
                <div className="field">
                    <div className="control">
                        <button className="button is-link">CONFIRM ADDRESS</button>
                    </div>
                </div>

                <div className="field">
                    <div className="control">
                        <a onClick={() => navigate('/')}>Back to previous</a>
                    </div>
                </div>
            </form>
        </div>
    );
}
