import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function BillingDetails() {
    const navigate = useNavigate();

    useEffect(() => {
        document.body.style.backgroundColor = "white";
    }, [])

    return (
        <div>
            <h1>BILLING DETAILS</h1>
            <hr />
            <div></div>
            <form action="">
                <div className="field">
                    <label className="label">NAME ON YOUR CARD</label>
                    <div className="control">
                        <input className="input" type="text" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">CARD NUMBER</label>
                    <div className="control">
                        <input className="input" type="text" placeholder="Type Here" />
                    </div>
                </div>

                <div className="field is-grouped">
                    <div className="field mr-5">
                        <label className="label">EXPIRATION DATE</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Type Here" />
                        </div>
                    </div>

                    <div className="field ml-5">
                        <label className="label">CVC</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Type Here" />
                        </div>
                    </div>
                </div>
            
                <div className="field">
                    <div className="control">
                        <button className="button is-link">CONFIRM PAYMENT</button>
                    </div>
                </div>

                <div className="field">
                    <div className="control">
                        <a onClick={() => navigate('/purchase/shipping-details')}>Back to previous</a>
                    </div>
                </div>
            </form>
        </div>
    );
}
