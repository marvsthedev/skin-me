import React, { useState } from 'react';
import {
  Link,
  Outlet
} from "react-router-dom";

export default function Layout() {
    const [showHambuger, setShowHambuger] = useState(false);

    return (
        <div>
            {/* A "layout route" is a good place to put markup you want to
                share across all the pages on your site, like navigation. */}
            <nav className="navbar" role="navigation" aria-label="main navigation">      
                <div className="navbar-brand">
                    <a role="button" className="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbar" onClick={() => setShowHambuger(!showHambuger)}>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>
            
                <div id="navbar" className={`navbar-menu ${showHambuger ? "is-active" : ""}`}>
                    <div className="navbar-start">
                        <a className="navbar-item">
                            <Link to="/">Home</Link>
                        </a>
                    </div>
                </div>
            </nav>
    
            <hr />
    
            {/* An <Outlet> renders whatever child route is currently active,
                so you can think about this <Outlet> as a placeholder for
                the child routes we defined above. */}
            <Outlet />
        </div>
    );
}
