import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import logo from '../assets/images/Yellow Logo FFBC00.svg'

import t from '../assets/images/image 60.svg'

export default function LandingPage() {
    const [isHovering, setIsHovering] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        document.body.style.backgroundColor = "#BDDEEE";
    }, [])


    const handleMouseOver = () => {
      setIsHovering(true);
    };
  
    const handleMouseOut = () => {
      setIsHovering(false);
    };

    return (
        <div id="landing-page">
            <h1>LANDING PAGE!!!!!</h1>

            <img src={logo} />

            <a
                className={isHovering ? 'button is-white' : 'button is-black'}
                onMouseOver={handleMouseOver}
                onMouseOut={handleMouseOut}
                onClick={() => navigate('/purchase/shipping-details')}
            >
                Buy Now
            </a>
        </div>
    );
}
